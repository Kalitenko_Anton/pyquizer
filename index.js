const { Telegraf } = require('telegraf')
const {
    Extra,
    Markup,
    Stage,
    session
  } = Telegraf
const config = require('config')
const bot = new Telegraf(config.get('token'))
const SceneGenerator = require('./Scenes')
const curScene = new SceneGenerator()
const question1 = curScene.GenQuestion1()
const question2 = curScene.GenQuestion2()
const question3 = curScene.GenQuestion3()
const question4 = curScene.GenQuestion4()
const question5 = curScene.GenQuestion5()
const result = curScene.GenResult()

bot.use(Telegraf.log())

const stage = new Stage([question1, question2, question3, question4, question5, result])

bot.use(session())
bot.use(stage.middleware())

bot.start((ctx) => {
  ctx.replyWithSticker(`CAACAgIAAxkBAAEE2J1ikknYbvwqrcakft-xWrF34vz9rwAC6AQAAvPyjj-be41RjBUoYyQE`);
  ctx.reply(`Вітаю в PyQuizer боті! Щоб почати опитування натисніть /quize`);
});

bot.command('info', (ctx) => {
  ctx.reply(`Тебе звати ${ctx.from.first_name} ${ctx.from.last_name ?? ''}`);
  ctx.reply(`Ім'я розробника: Калитенко Антон \nНавчальний заклад: ФКІСІТ ім. В. Гетьмана \nГрупа: 472 \nТелеграм:  @izntuser`);
});

bot.command('quize', async (ctx) => {
  return ctx.scene.enter('question1');
});

bot.on('callback_query', async (ctx) => {
  console.log(ctx)
  console.log('/////////*******************')
  const answer = ctx.update.callback_query.data
  
  if (answer == 'again') {
      right[ctx.update.callback_query.from.id] = 0;
      ctx.scene.enter('question1')
  }
})

bot.launch() 