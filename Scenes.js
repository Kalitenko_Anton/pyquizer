const Scene = require('telegraf/scenes/base')
const { quizeOptions, againOptions } = require('./options')

let quizeStart = {};
let right = {};

class SceneGenerator {
    GenQuestion1 () {
        const question1 = new Scene('question1')
        
        question1.enter(async (ctx) => {
            quizeStart[ctx.message.chat.id] = new Date();
            await ctx.reply(`Запитання №1: Що таке пітон? \n\nA) Змія така; \nБ) Мова програмування; \nВ) Зброя розроблена в Україні проти окупанта; \nГ) Давньогрецький бог.`, quizeOptions)
        })
        question1.on('text', async (ctx) => {
            const answer = ctx.message.text;
            if (answer === 'Б' || answer === 'Б)' || answer === 'б') {
                right[ctx.message.chat.id] = 1;
                ctx.scene.enter('question2')
            } else {
                right[ctx.message.chat.id] = 0;
                ctx.scene.enter('question2')
            }
        })
        question1.on('callback_query', async (ctx) => {
            const answer = ctx.update.callback_query.data
            if (answer === 'Б' || answer === 'Б)' || answer === 'б') {
                right[ctx.update.callback_query.from.id] = 1;
                ctx.scene.enter('question2')
            } else {
                right[ctx.update.callback_query.from.id] = 0;
                ctx.scene.enter('question2')
            }
        })
        question1.on('message', (ctx) => ctx.reply('Давай краще відповідь на запинання)'))
        return question1
    }

    GenQuestion2 () {
        const question2 = new Scene('question2')
        question2.enter(async (ctx) => {
            await ctx.reply(`Запитання №2: За допомогою чого відбувається визначення блоку коду в Python? \n\nA) За допомогою фігурних дужок; \nБ) За допомогою НАТО; \nВ) За допомогою відступів; \nГ) За допомогою чорної магії.`, quizeOptions)
        })
        question2.on('text', async (ctx) => {
            const answer = ctx.message.text;
            if (answer === 'В' || answer === 'В)' || answer === 'в') {
                right[ctx.message.chat.id]++;
                ctx.scene.enter('question3')
            } else {
                ctx.scene.enter('question3')
            }
        })
        question2.on('callback_query', async (ctx) => {
            const answer = ctx.update.callback_query.data
            if (answer === 'В' || answer === 'В)' || answer === 'в') {
                right[ctx.update.callback_query.from.id]++;
                ctx.scene.enter('question3')
            } else {
                ctx.scene.enter('question3')
            }
        })
        question2.on('message', (ctx) => ctx.reply('Давай краще відповідь на запинання)'))
        return question2
    }

    GenQuestion3 () {
        const question3 = new Scene('question3')
        question3.enter(async (ctx) => {
            await ctx.reply(`Запитання №3: Що виведе даний код? \nbool = '5' == 5 \nprint(bool) \n\nA) Виведе True; \nБ) Виведе False; \nВ) Виведе рашиські війська з України; \nГ) Виведе 5.`, quizeOptions)
        })
        question3.on('text', async (ctx) => {
            const answer = ctx.message.text;
            if (answer === 'Б' || answer === 'Б)' || answer === 'б') {
                right[ctx.message.chat.id]++;
                ctx.scene.enter('question4')
            } else {
                ctx.scene.enter('question4')
            }
        })
        question3.on('callback_query', async (ctx) => {
            const answer = ctx.update.callback_query.data
            if (answer === 'Б' || answer === 'Б)' || answer === 'б') {
                right[ctx.update.callback_query.from.id]++;
                ctx.scene.enter('question4')
            } else {
                ctx.scene.enter('question4')
            }
        })
        question3.on('message', (ctx) => ctx.reply('Давай краще відповідь на запинання)'))
        return question3
    }
    
    GenQuestion4 () {
        const question4 = new Scene('question4')
        question4.enter(async (ctx) => {
            await ctx.reply(`Запитання №4: За допомогою якого ключового слова створюється клас об'єкта? \n\nA) Ключового слова class; \nБ) Ключового слова Obj; \nВ) Ключового слова ПНХ; \nГ) Ключового слова create.`, quizeOptions)
        })
        question4.on('text', async (ctx) => {
            const answer = ctx.message.text;
            if (answer === 'А' || answer === 'А)' || answer === 'а') {
                right[ctx.message.chat.id]++;
                ctx.scene.enter('question5')
            } else {
                ctx.scene.enter('question5')
            }
        })
        question4.on('callback_query', async (ctx) => {
            const answer = ctx.update.callback_query.data
            if (answer === 'А' || answer === 'А)' || answer === 'а') {
                right[ctx.update.callback_query.from.id]++;
                ctx.scene.enter('question5')
            } else {
                ctx.scene.enter('question5')
            }
        })
        question4.on('message', (ctx) => ctx.reply('Давай краще відповідь на запинання)'))
        return question4
    }
    
    GenQuestion5 () {
        const question5 = new Scene('question5')
        question5.enter(async (ctx) => {
            await ctx.reply(`Запитання №5: Яка функція виводить щось у консоль? \n\nA) viydi_rozbiynik(); \nБ) log(); \nВ) print(); \nГ) write().`, quizeOptions)
        })
        question5.on('text', async (ctx) => {
            const answer = ctx.message.text;
            if (answer === 'В' || answer === 'В)' || answer === 'в') {
                right[ctx.message.chat.id]++;
                ctx.scene.enter('question3')
            } else {
                ctx.scene.enter('question3')
            }
        })
        question5.on('callback_query', async (ctx) => {
            const answer = ctx.update.callback_query.data
            if (answer === 'В' || answer === 'В)' || answer === 'в') {
                right[ctx.update.callback_query.from.id]++;
                ctx.scene.enter('result')
            } else {
                ctx.scene.enter('result')
            }
        })
        question5.on('message', (ctx) => ctx.reply('Давай краще відповідь на запинання)'))
        return question5
    }
    
    GenResult () {
        const result = new Scene('result')
        result.enter(async (ctx) => {
            let now = new Date();            
            let min = (now - quizeStart[ctx.update.callback_query.from.id]) / 60000;
            let sec = ((now - quizeStart[ctx.update.callback_query.from.id]) / 1000) - Number(String(min).split('.')[0]) * 60;
            await ctx.reply(`Опитувальник: ${ctx.update.callback_query.from.first_name} \nДата: ${now.getFullYear()}/${now.getMonth() + 1}/${now.getDate()} \nОцінка: ${right[ctx.update.callback_query.from.id]}/5 \nЗатрачено часу: ${String(min).split('.')[0]}хв. ${String(sec).split('.')[0]}сек.`)
            return ctx.reply(`/info`)
        })
        result.on('callback_query', async (ctx) => {
            const answer = ctx.update.callback_query.data
            if (answer == 'again') {
                await ctx.scene.enter('question1')
            }
        })
        return result
    }
}

module.exports = SceneGenerator