module.exports = {
    quizeOptions: {
       reply_markup: JSON.stringify({
           inline_keyboard: [
               [{text: 'А', callback_data: 'А'}, {text: 'В', callback_data: 'В'}],
               [{text: 'Б', callback_data: 'Б'}, {text: 'Г', callback_data: 'Г'}],
           ]
       })
   },

   againOptions: {
       reply_markup: JSON.stringify({
           inline_keyboard: [
               [{text: 'Спробувати ще раз', callback_data: 'again'}],
           ]
       })
   }
}